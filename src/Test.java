import com.company.Person;
import com.company.Test02;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Test {

    public static void main(String[] args){
        /**
         * Task 1
         */
        int a = 5;
        int b = 7;
        a = a + b;
        b = a - b;
        a = a - b;
        System.out.println("a = " + a + ", b = " + b);

        /**
         * Task 2
         */
        Test02 test02 = new Test02();
        String numberline = null;
        for (int i = 0; i < args.length; i++) {
            if (numberline == null) {
                numberline = args[i];
            } else {
                numberline = numberline + " " + args[i];
            }
        }
        test02.sort(numberline);

        /**
         * Task 3
         */
        try {
            List<String> lines = Files.readAllLines(Paths.get("d:\\JAVA\\STUDY\\test03.txt"), StandardCharsets.UTF_8);
            Set<Person> persons = new TreeSet<>();
            for (String line : lines){
                String[] data = line.split(" ");
                Person person = new Person(Integer.parseInt(data[2]), data[0], data[1]);
                persons.add(person);
            }
            System.out.println(persons);
        } catch (IOException e) {
            e.printStackTrace();
        }

        /**
         * Task 4
         */
        try {
            List<String> lines = Files.readAllLines(Paths.get("d:\\JAVA\\STUDY\\test03.txt"), StandardCharsets.UTF_8);
            Set<String> people = new HashSet<>(lines);
            List<String> names = new ArrayList<>();
            List<String> surnames = new ArrayList<>();
            List<String> ages = new ArrayList<>();
            for (String line : people){
                String[] data = line.split(" ");
                names.add(data[1]);
                surnames.add(data[0]);
                ages.add(data[2]);
            }
            Random rnd = new Random(System.nanoTime());
            Collections.shuffle(names, rnd);
            rnd = new Random(System.nanoTime());
            Collections.shuffle(surnames, rnd);
            rnd = new Random(System.nanoTime());
            Collections.shuffle(ages, rnd);
            Set<Person> persons = new TreeSet<>();
            for (int i = 0; i < names.size(); i++) {
                persons.add(new Person(Integer.parseInt(ages.get(i)), surnames.get(i), names.get(i)));
            }

            System.out.println(persons);
        } catch (IOException e) {
            e.printStackTrace();
        }

        /**
         * Task 5
         */
        try {
            List<String> lines = Files.readAllLines(Paths.get("d:\\JAVA\\STUDY\\error.txt"), StandardCharsets.UTF_8);
            try (FileWriter writer = new FileWriter("d:\\JAVA\\STUDY\\error_edit.txt", false)) {
                for (String line : lines){
                    for(int i = 0; i < line.length()-49; i = i + 50) {
                        System.out.println("Length of line: " + line.substring(i, i + 50).length() + ", line: " + line.substring(i, i + 50));
                        writer.write(line.substring(i, i + 50) + " ");
                        writer.append('\n');
                    }
                }
            }
            catch (IOException exception){
                System.out.println(exception.getMessage());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
