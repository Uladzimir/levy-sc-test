package com.company;

public class Test02 {
    public void sort(String numbers){
        String[] listNumbers = numbers.split(" ");
        int[] list = new int[listNumbers.length];
        for (int i = 0; i < listNumbers.length; i++) {
            try {
                list[i] = Integer.parseInt(listNumbers[i]);
            } catch (Exception exception){
                System.out.println(exception.getMessage());
                System.exit(1);
            }
        }
        boolean changed = true;
        while (changed){
            changed = false;
            for (int i = 1; i < list.length; i++) {
                if (list[i-1] < list[i]){
                    list[i-1] = list[i-1] + list[i];
                    list[i] = list[i-1] - list[i];
                    list[i-1] = list[i-1] - list[i];
                    changed = true;
                }
            }
        }
        System.out.println("Sorted array");
        for (int i = 0; i < list.length; i++) {
            System.out.print(list[i]);
        }
        System.out.println("");

    }
}
