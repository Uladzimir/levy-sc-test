package com.company;

import java.util.Objects;

public class Person implements Comparable{
    private int age;
    private String name1;
    private String name2;

    public Person(int age, String name1, String name2){
        this.age = age;
        this.name1 = name1;
        this.name2 = name2;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    @Override
    public int compareTo(Object o) {
        return this.name1.compareTo(((Person) o).name1);
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", name1='" + name1 + '\'' +
                ", name2='" + name2 + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age &&
                Objects.equals(name1, person.name1) &&
                Objects.equals(name2, person.name2);
    }

    @Override
    public int hashCode() {

        return Objects.hash(age, name1, name2);
    }
}
